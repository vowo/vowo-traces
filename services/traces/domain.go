package traces

import (
	"context"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/vowo/vowo-emissions/services/emissions"
)

// Repository provides access the traces store.
type Repository interface {
	GetByID(ctx context.Context, id uuid.UUID) (*Trace, error)
}

// Trace domain model
type Trace struct {
	ID                      uuid.UUID   `json:"id"`
	ProductID               uuid.UUID   `json:"productId"`
	ManufacturerID          uuid.UUID   `json:"manufacturerId"`
	QuantityInG             uint64      `json:"quantityInG"`
	Waypoints               *[]Waypoint `json:"waypoints"`
	TotalEmissionsCoTwoInMg uint64      `json:"totalEmissionsCoTwoInMg"`
	TotalDistanceInKm       float64     `json:"totalDistanceInKm"`
}

// Coord represents a geographic coordinate.
type Coord struct {
	Lat float64
	Lon float64
}

// Waypoint represents a travel point of a trace
type Waypoint struct {
	Emission      *emissions.Emission `json:"emission"`
	Source        *Coord              `json:"source"`
	Destination   *Coord              `json:"destination"`
	TransportType TransportType       `json:"transportType"`
}

type TransportType int

const (
	Other    TransportType = 0
	Truck    TransportType = 1
	Ship     TransportType = 2
	Airplane TransportType = 3
	Train    TransportType = 4
)

func (tt TransportType) String() string {
	names := [...]string{
		"Truck",
		"Ship",
		"Airplane",
		"Train",
	}

	if tt < Truck || tt > Train {
		return "Other"
	}

	return names[tt]
}
