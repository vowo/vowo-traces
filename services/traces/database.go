package traces

import (
	"context"
	"fmt"
	"sync"

	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"

	opentracing "github.com/opentracing/opentracing-go"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
)

type tracesRepository struct {
	mtx    sync.RWMutex
	traces map[uuid.UUID]*Trace
	tracer opentracing.Tracer
	logger log.Factory
}

func (r *tracesRepository) GetByID(ctx context.Context, id uuid.UUID) (*Trace, error) {
	r.mtx.RLock()
	defer r.mtx.RUnlock()
	if val, ok := r.traces[id]; ok {
		return val, nil
	}

	return nil, errors.Wrap(errNotFound, fmt.Sprintf("trace with id %v", id))
}

// newTracesRepository returns a new instance of a in-memory traces repository.
func newTracesRepository(tracer opentracing.Tracer, logger log.Factory) *tracesRepository {
	traceID1, _ := uuid.FromString("14cc2cae-3eb3-42a2-9ac8-662d943a1854")
	traceID2, _ := uuid.FromString("6fe57af3-4d0e-43a1-9b20-83291b7377b4")
	productID1, _ := uuid.FromString("a2226b8a-8100-4c44-8796-e86cd4175b0a")
	productID2, _ := uuid.FromString("cc669ef5-0d4d-4730-8f57-5ab7518b63f4")
	maufacturerID2, _ := uuid.FromString("49bf9ffc-d359-42db-b094-d56ba4b3c233")
	maufacturerID3, _ := uuid.FromString("6935f5ec-5ffb-402e-b076-aeffaeb3e066")
	waypoints := &[]Waypoint{
		0: Waypoint{
			Source: &Coord{
				Lat: 46.9480,
				Lon: 7.4474,
			},
			Destination: &Coord{
				Lat: 47.3769,
				Lon: 8.5417,
			},
			TransportType: Truck,
		},
	}

	waypoints1 := &[]Waypoint{
		0: Waypoint{
			Source: &Coord{
				Lat: 46.796852,
				Lon: 7.386071,
			},
			Destination: &Coord{
				Lat: 46.770605,
				Lon: 7.600367,
			},
			TransportType: Truck,
		},
		1: Waypoint{
			Source: &Coord{
				Lat: 46.770605,
				Lon: 7.600367,
			},
			Destination: &Coord{
				Lat: 46.805481,
				Lon: 7.507118,
			},
			TransportType: Truck,
		},
		2: Waypoint{
			Source: &Coord{
				Lat: 46.805481,
				Lon: 7.507118,
			},
			Destination: &Coord{
				Lat: 46.809672,
				Lon: 7.475314,
			},
			TransportType: Truck,
		},
		3: Waypoint{
			Source: &Coord{
				Lat: 46.809672,
				Lon: 7.475314,
			},
			Destination: &Coord{
				Lat: 46.948184,
				Lon: 7.444861,
			},
			TransportType: Truck,
		},
	}

	return &tracesRepository{
		tracer: tracer,
		logger: logger,
		traces: map[uuid.UUID]*Trace{
			traceID1: &Trace{
				ID:             traceID1,
				ProductID:      productID1,
				ManufacturerID: maufacturerID3,
				QuantityInG:    300,
				Waypoints:      waypoints1,
			},
			traceID2: &Trace{
				ID:             traceID2,
				ProductID:      productID2,
				ManufacturerID: maufacturerID2,
				QuantityInG:    395,
				Waypoints:      waypoints,
			},
		},
	}
}
