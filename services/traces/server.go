package traces

import (
	"net/http"
	"os"

	"gitlab.com/vowo/vowo-emissions/services/emissions"

	"github.com/heptiolabs/healthcheck"
	_ "github.com/joho/godotenv/autoload" //automatically load env variables
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-lib/metrics"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"go.uber.org/zap"
)

// Server implements traces service
type Server struct {
	hostPort         string
	healthHostPort   string
	tracer           opentracing.Tracer
	logger           log.Factory
	tracesRepository Repository
}

// NewServer creates a new traces server
func NewServer(hostPort string, healthHostPort string, tracer opentracing.Tracer, metricsFactory metrics.Factory, logger log.Factory, jAgentHostPort string) *Server {
	repo := newTracesRepository(
		tracing.Init("mysql", metricsFactory.Namespace("mysql", nil), logger, jAgentHostPort),
		logger.With(zap.String("component", "mysql")),
	)

	return &Server{
		hostPort:         hostPort,
		healthHostPort:   healthHostPort,
		tracer:           tracer,
		logger:           logger,
		tracesRepository: repo,
	}
}

// Run starts the traces server
func (s *Server) Run() error {
	var tracesService Service

	url := os.Getenv("EMISSIONS_URL")
	emissionsClient, err := emissions.NewHTTPClient(url, s.tracer, s.logger)
	if err != nil {
		return err
	}

	tracesService = NewService(s.tracer, s.logger, s.tracesRepository, emissionsClient)
	endpoint := NewEndpoint(tracesService, s.tracer, s.logger)
	mux := s.MakeHandler(endpoint)

	s.logger.Bg().Info("Starting", zap.String("address", "http://"+s.hostPort))
	return http.ListenAndServe(s.hostPort, mux)
}

// RunHealthAndReadinessProbes starts health and readiness probe
func (s *Server) RunHealthAndReadinessProbes() {
	health := healthcheck.NewHandler()

	//TODO implement proper checks
	// Our app is not happy if we've got more than 100 goroutines running.
	health.AddLivenessCheck("goroutine-threshold", healthcheck.GoroutineCountCheck(100))
	s.logger.Bg().Info("Starting Healthcheck", zap.String("address", "http://"+s.healthHostPort))
	err := http.ListenAndServe(s.healthHostPort, health)
	if err != nil {
		s.logger.Bg().Error("could not start readiness probe", zap.Error(err))
	}
}
