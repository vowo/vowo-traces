package traces

import (
	"context"

	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/endpoint"
	kitopentracing "github.com/go-kit/kit/tracing/opentracing"
	opentracing "github.com/opentracing/opentracing-go"
	uuid "github.com/satori/go.uuid"
	"github.com/sony/gobreaker"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
)

// getByIDRequest for an http request
type getByIDRequest struct {
	id uuid.UUID
}

// getByIDResponse to an http request
type getByIDResponse struct {
	Trace *Trace `json:"trace,omitempty"`
	Err   error  `json:"-"` // should be intercepted by Failed/errorEncoder
}

// Set collects all of the endpoints that compose an traces service.
type Set struct {
	getByID endpoint.Endpoint
}

// GetTraceByID implements the traces.Service
func (s *Set) GetTraceByID(ctx context.Context, id uuid.UUID) (*Trace, error) {
	resp, err := s.getByID(ctx, getByIDRequest{id: id})
	if err != nil {
		return nil, err
	}

	response := resp.(getByIDResponse)
	return response.Trace, response.Err
}

// NewEndpoint creates a new traces endpoint
func NewEndpoint(s Service, tracer opentracing.Tracer, logger log.Factory) *Set {
	var getByID endpoint.Endpoint
	{
		getByID = makeGetByIDEndpoint(s)
		getByID = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{}))(getByID)
		getByID = kitopentracing.TraceServer(tracer, "traces")(getByID)
		getByID = LoggingMiddleware(logger)(getByID)
	}

	return &Set{
		getByID: getByID,
	}
}

func makeGetByIDEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getByIDRequest)
		trace, err := s.GetTraceByID(ctx, req.id)

		if err != nil {
			return nil, err
		}

		return &getByIDResponse{Trace: trace, Err: err}, nil
	}
}

// compile time assertions for our response types implementing endpoint.Failer.
var (
	_ endpoint.Failer = getByIDResponse{}
)

// Failed implements endpoint.Failer.
func (r getByIDResponse) Failed() error { return r.Err }
