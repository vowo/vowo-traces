package cmd

import (
	"net"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-corelib/pkg/tracing"
	"gitlab.com/vowo/vowo-traces/services/traces"
	"go.uber.org/zap"
)

// tracesCommand represents the traces command
var tracesCommand = &cobra.Command{
	Use:   "traces",
	Short: "Starts traces service",
	Long:  `Starts traces service.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		zapLogger := logger.With(zap.String("service", "traces"))
		logger := log.NewFactory(zapLogger)
		server := traces.NewServer(
			net.JoinHostPort(tracesOptions.serverInterface, strconv.Itoa(tracesOptions.serverPort)),
			net.JoinHostPort(tracesOptions.serverInterface, strconv.Itoa(tracesOptions.serverHealthPort)),
			tracing.Init("traces", metricsFactory.Namespace("traces", nil), logger, jAgentHostPort),
			metricsFactory,
			logger,
			jAgentHostPort,
		)

		go server.RunHealthAndReadinessProbes()
		return logError(zapLogger, server.Run())
	},
}

var (
	tracesOptions struct {
		serverInterface  string
		serverPort       int
		serverHealthPort int
	}
)

func init() {
	RootCmd.AddCommand(tracesCommand)

	//TODO read port from env
	tracesCommand.Flags().StringVarP(&tracesOptions.serverInterface, "bind", "", "0.0.0.0", "interface to which the traces server will bind")
	tracesCommand.Flags().IntVarP(&tracesOptions.serverPort, "port", "p", 8084, "port on which the traces server will listen")
	tracesCommand.Flags().IntVarP(&tracesOptions.serverHealthPort, "healthPort", "e", 8184, "port on which the health check of the traces service will listen")
}
