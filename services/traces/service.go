package traces

import (
	"context"

	opentracing "github.com/opentracing/opentracing-go"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/vowo/vowo-corelib/pkg/log"
	"gitlab.com/vowo/vowo-emissions/services/emissions"
)

// Service is the interface to access traces.
type Service interface {
	GetTraceByID(ctx context.Context, id uuid.UUID) (*Trace, error)
}

type service struct {
	tracer           opentracing.Tracer
	logger           log.Factory
	tracesRepository Repository
	emissions        emissions.Service
}

func (s *service) GetTraceByID(ctx context.Context, id uuid.UUID) (*Trace, error) {
	trace, err := s.tracesRepository.GetByID(ctx, id)
	if err != nil {
		return nil, err
	}

	emissionsSum := uint64(0)
	totalDistanceInKm := float64(0)
	for i, waypoint := range *trace.Waypoints {
		wp := &emissions.Waypoint{
			Source:        &emissions.Coord{waypoint.Source.Lat, waypoint.Source.Lon},
			Destination:   &emissions.Coord{waypoint.Destination.Lat, waypoint.Destination.Lon},
			TransportType: emissions.TransportType(waypoint.TransportType),
		}

		emission, err := s.emissions.GetEmissionsForWaypoint(ctx, wp, trace.QuantityInG)
		if err != nil {
			return nil, err
		}

		(*trace.Waypoints)[i].Emission = emission
		emissionsSum += emission.CoTwoInMg
		totalDistanceInKm += emission.DistanceInKm
	}

	trace.TotalEmissionsCoTwoInMg = emissionsSum
	trace.TotalDistanceInKm = totalDistanceInKm
	return trace, nil
}

// NewService creates an traces service with necessary dependencies.
func NewService(tracer opentracing.Tracer, logger log.Factory, tracesRepository Repository, emissionsService emissions.Service) Service {
	return &service{
		logger:           logger,
		tracer:           tracer,
		tracesRepository: tracesRepository,
		emissions:        emissionsService,
	}
}
